<?php
App::uses('AppModel', 'Model');
/**
 * Annotation Model
 *
 * @property Tag $tag
 */
class Annotation extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'annotation';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'annotation_id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'tag' => array(
			'className' => 'Tag',
			'foreignKey' => false,
			'conditions' => 'tag.tag_id = Annotation.tag_id',
			'fields' => '',
			'order' => ''
		)
	);
}
