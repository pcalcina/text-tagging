<?php $this->Html->script('jquery-2.1.1.min.js',array('inline'=>false)); ?>

<style>
.status-confirmed {text-align:center; background-color:green; color:white};
</style>

<script>
function confirmNews(newsId){
	$.ajax({
        type: "POST",
        url: "<?php echo Router::url(array('controller' => 'news', 'action' => 'confirmNews')); ?>",
        data: {newsId:newsId},
        success: function(){
            $('#news-row-' + newsId + ' .status-unconfirmed' ).empty()
                .append('Confirmado').removeClass('actions').addClass('status-confirmed');
        }
    });
}
</script>

<div class="news index">
	<h2><?php echo __('Notícias'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('news_id', 'Id'); ?></th>
			<th><?php echo $this->Paginator->sort('title', 'Título'); ?></th>
			<th><?php echo $this->Paginator->sort('Source.name', 'Fonte'); ?></th>
			<th><?php echo $this->Paginator->sort('date', 'Data'); ?></th>
			<th class="xxxactions"><?php echo __('', ''); ?></th>
	</tr>
	<?php foreach ($news as $news): ?>
	<tr id="news-row-<?php echo h($news['News']['news_id']);?>">
		<td><?php echo h($news['News']['news_id']); ?>&nbsp;</td>
		<td><a href="<?php echo $this->Html->url(array('action' => 'annotate', $news['News']['news_id'])); ?>">
		       <?php echo h($news['News']['title']); ?>&nbsp; </a></td>
		<td> <?php echo h($news['Source']['name']); ?>  </td>
		<td><?php echo h($news['News']['date']); ?>&nbsp;</td>
		
		<?php if($news['News']['confirmed']): ?>
    		<td class="status-confirmed"> Confirmado </td>	
	    <?php else: ?>
    		<td class="actions status-unconfirmed"><a href="javascript:confirmNews(<?php echo $news['News']['news_id']; ?>)"> Confirmar </a></td>	    
	    <?php endif ?>

		<td class="actions">
<!--			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $news['News']['news_id'])); ?> -->
			<?php echo $this->Form->postLink(__('Apagar'), array('action' => 'delete', $news['News']['news_id']), null, __('Are you sure you want to delete # %s?', $news['News']['news_id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Ações'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Nova Notícia'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Gerenciar eventos'), array('controller' => 'events', 'action' => 'index')); ?></li>		
		<li><?php echo $this->Html->link(__('Gerenciar tags'), array('controller' => 'tags', 'action' => 'index')); ?></li>				
		<li><?php echo $this->Html->link(__('Gerenciar anotações'), array('controller' => 'annotations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nova anotação'), array('controller' => 'annotations', 'action' => 'add')); ?> </li>
	</ul>
</div>
