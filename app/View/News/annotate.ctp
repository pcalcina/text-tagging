<?php $this->Html->script('jquery-2.1.1.min.js',array('inline'=>false)); ?>
<?php $this->Html->script('select2.min.js',array('inline'=>false)); ?>
<?php $this->Html->script('tag-text.js',array('inline'=>false)); ?>
<?php $this->Html->script('jquery.qtip.min.js',array('inline'=>false)); ?>
<?php $this->Html->script('jquery.textHighlighter.js',array('inline'=>false)); ?>

<?php echo $this->Html->css('select2.css'); ?>
<?php echo $this->Html->css('jquery.qtip.min.css'); ?>

<script>
function collectAnnotations(){
	var annotations = [];
	$('tr', '#table_properties').each(function(i, row){
		annotations.push({
			news_id: '<?php echo $news['News']['news_id'] ?>',
			tag_id : $('.annotation-name', row).select2("val"),
			value  : $('.annotation-value', row).val(),
			annotation_id: $('.annotation-id', row).val()
		}); 
	});

	return {Annotation: annotations};
	
}

var savedAnnotations = <?php echo json_encode($annotations); ?>;

function saveAnnotations(){
	$.ajax({
        type: "POST",
        url: '<?php echo Router::url(array('controller' => 'annotations', 'action' => 'saveAjax')); ?>',
        data: collectAnnotations(),
        success: function(annotations){
            fillProperties(annotations);
        },
        dataType: 'json'
    });
}

function removeAnnotation(row){
	var annotationId = $('.annotation-id', row).val();
	
	if(annotationId){
		$.ajax({
	        type: "POST",
	        url: "<?php echo Router::url(array('controller' => 'annotations', 'action' => 'deleteAjax')); ?>",
	        data: {id: annotationId},
	        success: function(annotations){
			
	        }
	    });	
	}
	row.remove();
}
</script>

<div class="news form">

<table style="width:100%">
    <tr>
    <td>
    <h2>Anotar notícia</h2>
	    <div class="actions" style="width:100%">
		    <b>Selecione um trecho de texto para anotá-lo</b>
	    </div>

        <select class="options-tags" id="options-tags" style="display:none; width:100%">
	    <?php foreach ($tags as $tag): ?>
		    <option value="<?php echo h($tag['Tag']['tag_id']); ?>"><?php echo h($tag['Tag']['name']); ?></option>
	    <?php endforeach; ?>
        </select>
        
        </td>
        
        <td style='vertical-align:bottom; padding-bottom:12px'>            &nbsp;&nbsp;&nbsp;
            <span class='actions'>
            <a href="javascript:addInputProperty()"> Adicione anotação manualmente</a> &nbsp;
            <a href="javascript:saveAnnotations()"> Salvar anotações </a> 
            </span> <br/>
        </td>
    </tr>
    
	<tr>
		<td style="width:50%">
		
		
        
		<div id="texto-principal" style="border: 2px solid; border-radius:10px; margin: 10px; padding:10px">
		    <span> Data: <b><?php echo h($news['News']['date']) ?></b> &nbsp; Fonte: <b><?php echo h($news['Source']['name']); ?> </b> &nbsp;
		    Palavras chave: <b><?php echo h($news['News']['keywords']) ?> </b>
		    </span> <br/> </br>
		    
			<h3><?php echo h($news['News']['title']) ?></h3>
			
			<div><?php echo $news['News']['content']; ?></div>
		</div>
		
		</td>
	
		<td style="width:*; vertical-align:top;">
<!--			<h4>Anotações realizadas</h4>  -->
            
            <span>
                <b> Evento associado: </b> &nbsp;
	            <select id='select-evento-associado' style='width:50%'>
	                <?php foreach ($events as $event): ?>
	                    <option value="<?php echo h($event['Event']['event_id']); ?>"><?php echo h($event['Event']['name']); ?></option>
                    <?php endforeach; ?>
                </select>
            </span>

			
			<table style="width:100%">
				<thead>
					<tr>
						<th style="width:150px">
						</th >
						
						<th style="width:*">
						</th >
						
						<th style="width:25px">
						</th >
					</tr>
				</thead>
				
				<tbody id="table_properties">
				</tbody>
			</table>
		</td>	
	</tr>
</table>
</div>

<div class="actions">
	<h3><?php echo __('Ações'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Ver todas as notícias'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Gerenciar eventos'), array('controller' => 'events', 'action' => 'index')); ?></li>	
		<li><?php echo $this->Html->link(__('Gerenciar tags'), array('controller' => 'tags', 'action' => 'index')); ?></li>				
		<li><?php echo $this->Html->link(__('Nova tag'), array('controller' => 'tags', 'action' => 'add')); ?></li>
<!--		<li><a href="javascript:exportAnotationsCSV()"> Exportar em CSV </a> </li> -->
<!--  <li><?php echo $this->Form->postLink(__('Apagar notícia'), array('action' => 'delete', $this->Form->value('News.news_id')), null, __('Apagar notícia?', $this->Form->value('News.news_id'))); ?></li> -->
		<!-- <li><?php echo $this->Html->link(__('Nova anotação'), array('controller' => 'annotations', 'action' => 'add')); ?> </li> -->
	</ul>

    <br><br><br>
    <h3><b>Etiquetas disponíveis</b></h3>
    
    <?php echo $this->Html->link(__('Saiba mais'), array('controller' => 'tags', 'action' => 'description')); ?> <br/> <br/>
    
	<table>
	    <?php foreach ($tags as $tag): ?>
		    <tr><td id="tag-<?php echo h($tag['Tag']['tag_id']); ?>" style="margin:1px; padding:1px; color:#191970;" ><b><?php echo h($tag['Tag']['name']); ?></b></td></tr>
	    <?php endforeach; ?>
	</table>
</div>
