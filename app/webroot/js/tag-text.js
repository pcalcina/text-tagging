var attributes = {};

$(document).ready(function() {

	$('#texto-principal').textHighlighter({
		onAfterHighlight: function(highlights, text) {
			
			if (text.length > 0) {
				addInputProperty({text:text, highlights:highlights});
			}
        }
	});
	
	$('.annotation-name').on('change', function(){
	    alert($(this).val());
	});
	
	fillProperties(savedAnnotations);
	
	$('#select-evento-associado').select2();
});

function fillProperties(annotations){
    $('#table_properties').empty();
    
    for(var i in annotations){
		var annotation = annotations[i].Annotation;
		var tag = annotations[i].tag;
		
		addInputProperty({selectOpen:false, 
						  text: annotation.value, 
						  selectedTag: annotation.tag_id, 
						  annotationId: annotation.annotation_id})
	}
	
	
}

function addInputProperty(options){
	options = typeof(options) == "undefined"? {} : options;
	options.text = typeof(options.text) == "undefined"? '' : options.text;
	options.highlights = typeof(options.highlights) == "undefined"? null : options.highlights;
	options.selectOpen = typeof(options.selectOpen) == "undefined"? true : options.selectOpen;
	options.selectedTag = typeof(options.selectedTag) == "undefined"? null : options.selectedTag;
	options.annotationId = typeof(options.annotationId) == "undefined"? null : options.annotationId;
		
	var inputPropertyName = $("<input>");
	inputPropertyName.css("width", "100%");
	inputPropertyName.css("font-size", "11pt")
	inputPropertyName.val(options.text);
	inputPropertyName.addClass('annotation-value');
	inputPropertyName.focus();
	
	var optionsTags = $("#options-tags").clone();
	optionsTags.show();
	optionsTags.css("width", '150px');
	optionsTags.addClass('annotation-name');
	
	var row = $("<tr>");
	
	var btnRemove = $("<input>").prop("type", "button")
		.css("color", "red").val("x");
	
	btnRemove.click(function(){
		if(options.highlights){
			$('#texto-principal').getHighlighter().removeHighlights(options.highlights);
		}
		
		removeAnnotation(row);
	});
	
	$("#table_properties").append(
		row
			.append($("<td>").append(optionsTags))
			.append($("<td>")
				.append(inputPropertyName))
			.append($("<td>").append(btnRemove))
	);
	
	optionsTags.focus();
	
	optionsTags.select2();
	
	if(options.selectedTag){
		optionsTags.select2("val", options.selectedTag);
	}
	
	if(options.selectOpen){
		optionsTags.select2('open');
	}
	
	var inputAnnotationId = $("<input>").prop("type", "hidden").addClass('annotation-id');
	row.append(inputAnnotationId);
	
	if(options.annotationId){
		inputAnnotationId.val(options.annotationId);
	}
}

function getSelectedText() {
	if (window.getSelection) {
		return window.getSelection().toString();
	} 
	else if (document.getSelection) {
		return document.getSelection();
	} 
	else if (document.selection) {
		return document.selection.createRange().text;
	}
}

