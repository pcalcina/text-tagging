<?php
App::uses('AppController', 'Controller');
/**
 * Annotations Controller
 *
 * @property Annotation $Annotation
 * @property PaginatorComponent $Paginator
 */
class AnnotationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Annotation->recursive = 0;
		$this->set('annotations', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Annotation->exists($id)) {
			throw new NotFoundException(__('Invalid annotation'));
		}
		$options = array('conditions' => array('Annotation.' . $this->Annotation->primaryKey => $id));
		$this->set('annotation', $this->Annotation->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			
			$this->Annotation->create();

			if ($this->Annotation->save($this->request->data)) {
				$this->Session->setFlash(__('The annotation has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} 
			else {
				$this->Session->setFlash(__('The annotation could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Annotation->exists($id)) {
			throw new NotFoundException(__('Invalid annotation'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Annotation->save($this->request->data)) {
				$this->Session->setFlash(__('The annotation has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The annotation could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Annotation.' . $this->Annotation->primaryKey => $id));
			$this->request->data = $this->Annotation->find('first', $options);
		}
	}
	
	public function news($newsId = null){
		$this->set('annotations', $this->loadAnnotationsForNews($newsId));
	}
	
	protected function loadAnnotationsForNews($newsId){
		$options = array('conditions' => array('Annotation.news_id' => $newsId));
		return $this->Annotation->find('all', $options);
	}
	
	public function saveAjax(){
		$this->layout = "ajax";
		
		foreach($this->request->data['Annotation'] as $annotation){
			$annotationToSave = array('Annotation' => $annotation);
			$this->Annotation->create();
			
			if(empty($annotationToSave['Annotation']['annotation_id'])){
				unset($annotationToSave['Annotation']['annotation_id']);
			}
			
			$this->Annotation->save($annotationToSave);
		}		
		
		if(!empty($this->request->data['Annotation'])){
    		$optionsAnnotations= array('conditions' => array('Annotation.news_id' => 
    		    $this->request->data['Annotation'][0]['news_id']));
    		$this->set('annotations', $this->Annotation->find('all', $optionsAnnotations));
		}
	}
	
	public function deleteAjax(){
		$this->layout = "ajax";
		
		$this->Annotation->id = $this->request->data['id'];

		if ($this->Annotation->exists()) {
			$this->Annotation->delete();
		}
	}
	

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Annotation->id = $id;
		if (!$this->Annotation->exists()) {
			throw new NotFoundException(__('Invalid annotation'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Annotation->delete()) {
			$this->Session->setFlash(__('The annotation has been deleted.'));
		} else {
			$this->Session->setFlash(__('The annotation could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
